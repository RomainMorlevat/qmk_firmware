#include QMK_KEYBOARD_H

// Emoji usage from https://gitmoji.dev/
enum unicode_names {
    API,
    BUG,
    CONFIG,
    DATABASE,
    FEATURE,
    HOTFIX,
    LINT,
    LOCALE,
    POOP,
    REFACTO,
    REMOVECODE,
    STYLE,
    TESTS,
    TYPO,
};

const uint32_t PROGMEM unicode_map[] = {
    [API] = 0x1F47D,  // 👽️
    [BUG]  = 0x1F41B,  // 🐛
    [CONFIG] = 0x1F527,  // 🔧
    [DATABASE] = 0x1F5C3, // 🗃️
    [FEATURE] = 0x2728,  // ✨
    [HOTFIX] = 0x1F691,  // 🚑️
    [LINT] = 0x1F6A8,  // 🚨
    [LOCALE] = 0x1F310,  // 🌐
    [POOP] = 0x1F4A9, // 💩
    [REFACTO] = 0x267B,  // ♻️
    [REMOVECODE] = 0x1F525,  // 🔥
    [STYLE] = 0x1F484,  // 💄
    [TESTS]  = 0x2705, // ✅
    [TYPO] = 0x270F,  // ✏️
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	[0] = LAYOUT(X(API), LT(1,KC_PSCR), X(BUG), X(TESTS), LT(2,KC_CALC), X(TYPO), X(FEATURE), LT(3, KC_MUTE), X(HOTFIX)),
	[1] = LAYOUT(X(REFACTO), KC_NO, X(POOP), X(LINT), X(CONFIG), X(DATABASE), X(REMOVECODE), X(LOCALE), X(STYLE)),
	[2] = LAYOUT(RGB_RMOD, RGB_VAI, RGB_MOD, RGB_HUI, KC_NO, RGB_SAI, RGB_HUD, RGB_VAD, RGB_SAD),
	[3] = LAYOUT(RGB_M_B, RGB_M_G, RGB_M_SW, RGB_M_SN, RGB_M_R, RGB_M_K, RGB_M_X, KC_NO, RGB_TOG)
};

